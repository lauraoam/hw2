function addTokens(input, tokens){
    if(typeof input !== 'string')
    {
        throw new Error('Invalid input');
    }
    if(input.length<6)
    {
    	throw new Error('Input should have at least 6 characters');
    }

    if(tokens.every(function(i){return typeof i.tokenName==='string'})===false)
    {
    	throw new Error('Invalid array format');
    }
    if(input.includes('...')===false)
    {
        return input;
    } else
    {
    	var sinput=input.split(" ");

        var j=0;
    	for(var i=0; i<sinput.length; i++)
    	{
    		if(sinput[i]==='...')
    		{
    			var a=tokens[j].tokenName;
    			sinput[i]="${"+a+"}";
    			j++;
    		}
    	}
    	input=sinput.join(" ");
    	return input;
    }

   
}

const app = {
    addTokens: addTokens
}

module.exports = app;


// var str='Subsemnatul ... fgfg ...';
// var t=[{a: 1}, {a:2}];
// var sinput=str.split(" ");
// var j=0;
// for(var i=0; i<sinput.length; i++){
// {if(sinput[i]==='...')
//     		{
//     			var b=t[j].a;
//     			sinput[i]="${"+b+"}";
//     			j++;
//     		}
//  }
// }
// str=sinput.join(" ");

	//console.log(str);




